import { Component, OnInit, Input } from '@angular/core';
import { Cubo } from '../clases/cubo';

@Component({
  selector: 'app-cara',
  templateUrl: './cara.component.html',
  styleUrls: ['./cara.component.css']
})
export class CaraComponent implements OnInit {


@Input()posicion: number;
@Input()cubo: Cubo;

  constructor() {
   }

  ngOnInit() {
  }


}
