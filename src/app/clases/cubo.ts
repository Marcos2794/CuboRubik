import { Cara } from './cara';
import { Lado } from '../lado.enum';
export class Cubo {
    caras: Cara[];
    micara: number;
    constructor() {
        this.caras = [
        new Cara('yellow'),
        new Cara('blue'),
        new Cara('white'),
        new Cara('green'),
        new Cara('red'),
        new Cara('orange')];
    }

    giraizquierda(celizq: number, cara: string, fila: string) {
        const original = this.caras[Lado.Frente].celdas[celizq];
        this.caras[Lado.Frente].celdas[celizq] = this.caras[Lado.Derecha].celdas[celizq];
        this.caras[Lado.Derecha].celdas[celizq] = this.caras[Lado.Atras].celdas[celizq];
        this.caras[Lado.Atras].celdas[celizq] = this.caras[Lado.Izquierda].celdas[celizq];
        this.caras[Lado.Izquierda].celdas[celizq] = original;
        if (cara !== 'red' && fila === 'Arriba' || cara !== 'orange' && fila === 'Arriba') {
            this.girocara(Lado.Arriba);
        } else if (cara !== 'red' && fila === 'Abajo' || cara !== 'orange' && fila === 'Abajo') {
            this.girocara(Lado.Abajo);
        }
  }
  giraderecha(celder: number, cara: string, fila: string) {
      for (let index = 0; index < 3; index++) {
          this.giraizquierda(celder, cara, fila);
      }
    }

giraarriba(fila1: number, fila2: number, cara: string, auxilia: string) {
        if (cara === 'green' && auxilia !== 'orange' ) { // esta linea valida si las celdas a girar son las verdes o azules
            this.giraverde(fila1, fila2);
            if (cara === 'green' && fila1 === 0 ) {
                this.girocara(Lado.Frente);
            }  else {
                for (let index = 0; index < 3; index++) {
                this.girocara(Lado.Atras);
            }
            }
        } else if (cara === 'green' && auxilia === 'orange' ) { // esta linea valida si las celdas a girar son las verdes o azules
            this.giraverde(fila1, fila2);
            if (cara === 'green' && fila1 === 0 ) {
                for (let index = 0; index < 3; index++) {
                this.girocara(Lado.Frente);
            }
            }  else {
                this.girocara(Lado.Atras);
            }
        } else if (cara === 'blue' ) { // esta linea valida si las celdas a girar son las verdes o azules
            for (let index = 0; index < 3; index++) {
            this.giraverde(fila1, fila2);
            }
            if (cara === 'blue' && fila1 === 0 ) {
                for (let index = 0; index < 3; index++) {
                    this.girocara(Lado.Frente);
                    }
            }  else {
                this.girocara(Lado.Atras);
            }
        } else {
            if ( fila1 === 0 && cara !== 'white' ) {
                    for (let index = 0; index < 3; index++) {
                    this.girocara(Lado.Izquierda);
                }
            this.giroamarillo(fila1, fila2);
            } else if ( fila1 === 2 && cara !== 'white' ) {
                    this.girocara(Lado.Derecha);
            this.giroamarillo(fila1, fila2);
            } else if ( fila1 === 0 && cara === 'white' ) {
                    this.girocara(Lado.Izquierda);
                    for (let index = 0; index < 3; index++) {
                    this.giroamarillo(fila1, fila2);
                }
            } else if ( fila1 === 2 && cara === 'white' ) {
                for (let index = 0; index < 3; index++) {
                this.girocara(Lado.Derecha);
                this.giroamarillo(fila1, fila2);
            }
            }
    }
  }

  giraabajo(fila1: number, fila2: number, cara: string, auxiliar: string) {
    for (let index = 0; index < 3; index++) {
        this.giraarriba(fila1, fila2, cara, auxiliar);
    }
}
// // metodo para girar las todas las celdas menos las verdes y azules hacia arriba o abajo
  giroamarillo(fila1: number, fila2: number) {
    const original = this.caras[Lado.Frente].celdas[0][fila1];
    const original1 = this.caras[Lado.Frente].celdas[1][fila1];
    const original2 = this.caras[Lado.Frente].celdas[2][fila1];
    this.caras[Lado.Frente].celdas[0][fila1] = this.caras[Lado.Abajo].celdas[2][fila1];
    this.caras[Lado.Frente].celdas[1][fila1] = this.caras[Lado.Abajo].celdas[1][fila1];
    this.caras[Lado.Frente].celdas[2][fila1] = this.caras[Lado.Abajo].celdas[0][fila1];
    this.caras[Lado.Abajo].celdas[0][fila1] = this.caras[Lado.Atras].celdas[0][fila2];
    this.caras[Lado.Abajo].celdas[1][fila1] = this.caras[Lado.Atras].celdas[1][fila2];
    this.caras[Lado.Abajo].celdas[2][fila1] = this.caras[Lado.Atras].celdas[2][fila2];
    this.caras[Lado.Atras].celdas[0][fila2] = this.caras[Lado.Arriba].celdas[2][fila1];
    this.caras[Lado.Atras].celdas[1][fila2] = this.caras[Lado.Arriba].celdas[1][fila1];
    this.caras[Lado.Atras].celdas[2][fila2] = this.caras[Lado.Arriba].celdas[0][fila1];
    this.caras[Lado.Arriba].celdas[0][fila1] = original;
    this.caras[Lado.Arriba].celdas[1][fila1] = original1;
    this.caras[Lado.Arriba].celdas[2][fila1] = original2;
  }

  // metodo para girar las celdas verdes y azules hacia arriba o abajo
  giraverde(fila1: number, fila2: number) {
    const original = this.caras[Lado.Izquierda].celdas[2][fila2];
    const original1 = this.caras[Lado.Izquierda].celdas[1][fila2];
    const original2 = this.caras[Lado.Izquierda].celdas[0][fila2];
    this.caras[Lado.Izquierda].celdas[0][fila2] = this.caras[Lado.Abajo].celdas[fila2][0];
    this.caras[Lado.Izquierda].celdas[1][fila2] = this.caras[Lado.Abajo].celdas[fila2][1];
    this.caras[Lado.Izquierda].celdas[2][fila2] = this.caras[Lado.Abajo].celdas[fila2][2];
    this.caras[Lado.Abajo].celdas[fila2][0] = this.caras[Lado.Derecha].celdas[2][fila1];
    this.caras[Lado.Abajo].celdas[fila2][1] = this.caras[Lado.Derecha].celdas[1][fila1];
    this.caras[Lado.Abajo].celdas[fila2][2] = this.caras[Lado.Derecha].celdas[0][fila1];
    this.caras[Lado.Derecha].celdas[0][fila1] = this.caras[Lado.Arriba].celdas[fila2][0];
    this.caras[Lado.Derecha].celdas[1][fila1] = this.caras[Lado.Arriba].celdas[fila2][1];
    this.caras[Lado.Derecha].celdas[2][fila1] = this.caras[Lado.Arriba].celdas[fila2][2];
    this.caras[Lado.Arriba].celdas[fila2][0] = original;
    this.caras[Lado.Arriba].celdas[fila2][1] = original1;
    this.caras[Lado.Arriba].celdas[fila2][2] = original2;
}

// metodo para girar toda la cara roja
girocara(lado: number) {
    const original = this.caras[lado].celdas[0][0];
    const original1 = this.caras[lado].celdas[1][0];
    const original2 = this.caras[lado].celdas[2][0];
    this.caras[lado].celdas[1][0] = this.caras[lado].celdas[2][1];
    this.caras[lado].celdas[0][0] = this.caras[lado].celdas[2][0];
    this.caras[lado].celdas[2][0] = this.caras[lado].celdas[2][2];
    this.caras[lado].celdas[2][1] = this.caras[lado].celdas[1][2];
    this.caras[lado].celdas[2][2] = this.caras[lado].celdas[0][2];
    this.caras[lado].celdas[1][2] = this.caras[lado].celdas[0][1];
    this.caras[lado].celdas[0][2] = original;
    this.caras[lado].celdas[0][1] = original1;
}

}
