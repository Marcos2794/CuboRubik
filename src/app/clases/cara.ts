import { Celda } from './celda';

export class Cara {
    celdas: Celda[][];
    constructor(color: string) {
        this.celdas = [[new Celda(color), new Celda(color), new Celda(color)],
        [new Celda(color), new Celda(color), new Celda(color)],
        [new Celda(color), new Celda(color), new Celda(color)]
    ];
    }
  }
