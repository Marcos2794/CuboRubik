export class Movimiento {
    gira: string;
    posfila: string;
    cara: string;
    numfila1: number;
    numfila2: number;
    constructor(giro: string, posfila: string, cara: string, numfila1: number, numfila2: number) {
        this.gira = giro;
        this.posfila = posfila;
        this.cara = cara;
        this.numfila1 = numfila1;
        this.numfila2 = numfila2;
    }
  }
