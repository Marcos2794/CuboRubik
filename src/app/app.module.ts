import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CaraComponent } from './cara/cara.component';
import { CeldaComponent } from './celda/celda.component';
import { CuboComponent } from './cubo/cubo.component';

@NgModule({
  declarations: [
    AppComponent,
    CaraComponent,
    CeldaComponent,
    CuboComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
