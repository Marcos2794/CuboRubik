import { Component, OnInit } from '@angular/core';
import { Cubo } from '../clases/cubo';
import { Movimiento } from '../clases/movimientos';

@Component({
  selector: 'app-cubo',
  templateUrl: './cubo.component.html',
  styleUrls: ['./cubo.component.css']
})
export class CuboComponent implements OnInit {

public nuevomovimiento: Movimiento[];
public guardar: Movimiento[];
cubo: Cubo;

public columna: number;
public regresarojo: boolean;
public regresar: boolean;
public restaura: boolean;
public guardanuevo: boolean;
public Fila = '';
public Cara = '';
public Auxiliarcara = '';
public Auxiliarfila = '';
public Auxiliarcambio = '';
public fila1 = 0;
public fila2 = 0;
public giro = '';
public izqder = '';


  constructor() {
    this.cubo = new Cubo;
    this.giro = 'giraamarillo';
    this.Cara = 'yellow';
    this.Fila = 'Arriba';
    this.nuevomovimiento = [];
    this.regresar = false;
    this.regresarojo = false;
    localStorage.removeItem('movimientos');
   }

  ngOnInit() {
  }
   // metodo que captura la fila seleccionada
FilaSeleccionada (event: any) {
    this.Fila = event.target.value;
    this.seleccionizqder();
}

 // metodo para seleccionar cara a mover
CaraSeleccionada(event: any) {
  this.Cara = event.target.value;
  this.seleccionizqder();
}

seleccionizqder() {
  if ( this.Cara === 'green') {
    if (this.Fila === 'Izquierda') {
      this.fila1 = 2;
      this.fila2 = 0;
      }  else {
        this.fila1 = 0;
        this.fila2 = 2;
      }
  } else if ( this.Cara === 'blue') {
    if (this.Fila === 'Izquierda') {
      this.fila1 = 0;
      this.fila2 = 2;
      }  else {
        this.fila1 = 2;
        this.fila2 = 0;
      }
  } else if ( this.Cara === 'white') {
    if (this.Fila === 'Izquierda') {
      this.fila1 = 2;
      this.fila2 = 0;
      }  else {
        this.fila1 = 0;
        this.fila2 = 2;
      }
  }  else {
    if (this.Fila === 'Izquierda') {
      this.fila1 = 0;
      this.fila2 = 2;
      }  else {
        this.fila1 = 2;
        this.fila2 = 0;
      }
  }
}

  giraizquierda() {
    this.Auxiliarcara = this.Cara;
    this.Auxiliarfila = this.Fila;
    this.izqder = 'izquierda';
    if ( this.Cara === 'red' || this.Cara === 'orange') {
      this.girorojonaranja();
      if (this.regresarojo === false) {
        this.nuevomovimiento.pop();
        this.guardamovimiento(this.izqder);
        this.guardanuevo = true;
        } else {
          this.regresar = true;
          this.regresarojo = true;
          this.guardamovimiento(this.izqder);
          this.guardanuevo = true;
          }
    } else {
        if (this.Fila === 'Arriba') {
        this.cubo.giraizquierda(0, this.Cara, this.Fila);
        this.guardanuevo = true;
        } if (this.Fila === 'Abajo') {
          this.cubo.giraizquierda(2, this.Cara, this.Fila);
          this.guardanuevo = true;
        }
    this.guardamovimiento(this.izqder);
    }
    this.borramovimiento();
  }
  giraderecha() {
    this.Auxiliarcara = this.Cara;
    this.Auxiliarfila = this.Fila;
    this.izqder = 'derecha';
    if ( this.Cara === 'red' || this.Cara === 'orange') {
      this.girorojonaranja();
      if (this.regresarojo === false) {
        this.nuevomovimiento.pop();
        this.guardamovimiento(this.izqder);
        this.guardanuevo = true;
        } else {
          this.regresar = true;
          this.regresarojo = true;
          this.guardamovimiento(this.izqder);
          this.guardanuevo = true;
          }
    } else {
      if (this.Fila === 'Arriba') {
        this.cubo.giraderecha(0, this.Cara, this.Fila);
        this.guardanuevo = true;
        } if (this.Fila === 'Abajo') {
          this.cubo.giraderecha(2, this.Cara, this.Fila);
          this.guardanuevo = true;
        }
    this.guardamovimiento(this.izqder);
    }
    this.borramovimiento();
    }
    // metodo para girar las cara roja y naranja de izquierda a derecha y viceversa
    girorojonaranja() {
      if (this.Cara === 'orange'  && this.Fila === 'Arriba') {
        this.Auxiliarfila = 'Abajo';
        this.Auxiliarcambio = 'Arriba';
      } else if (this.Cara === 'red') {
            if (this.Fila === 'Abajo') {
              this.Auxiliarfila = 'Abajo';
              this.Auxiliarcambio = 'Abajo';
            } else {
              this.Auxiliarfila = 'Arriba';
              this.Auxiliarcambio = 'Arriba';
            }
      } else {
        this.Auxiliarfila = 'Arriba';
        this.Auxiliarcambio = 'Abajo';
      }
      this.Cara = 'green';
      if ( this.Auxiliarfila === 'Arriba') {
          this.Fila = 'Izquierda';
          this.seleccionizqder();
          if (this.izqder === 'izquierda') {
            this.giraabajo(); // en la cara roja seria jiro a la izquierda
          } else {
            this.giraarriba();
          }
      } else {
          this.Fila = 'Derecha';
          this.seleccionizqder();
          if (this.izqder === 'izquierda') {
            this.giraabajo(); // en la cara roja seria jiro a la derecha
          } else {
            this.giraarriba();
          }
      }
      this.Cara = this.Auxiliarcara;
      this.Auxiliarfila = this.Auxiliarcambio;
      this.Fila = this.Auxiliarfila;
    }
    // metodo para resetear
    reset() {
    this.cubo = new Cubo;
    this.Fila = 'Arriba';
    this.Cara = 'yellow';
    this.giro = 'amarillo';
    this.restaura = false;
    this.nuevomovimiento = [];
    localStorage.clear();
    }

    //  metodo para girar hacia arriba
    giraarriba() {
      if ( this.Fila === 'Derecha' || this.Fila === 'Izquierda') {
        this.cubo.giraarriba(this.fila1, this.fila2, this.Cara, this.Auxiliarcara);
        this.guardamovimiento('arriba');
        this.guardanuevo = true;
      }
      this.borramovimiento();
    }

    // metodo para girar hacia abajo
    giraabajo() {
      if ( this.Fila === 'Derecha' || this.Fila === 'Izquierda') {
      this.cubo.giraabajo(this.fila1, this.fila2, this.Cara, this.Auxiliarcara);
      this.guardamovimiento('abajo');
      this.guardanuevo = true;
      }
      this.borramovimiento();
    }
      // Metodo para cambiar las vista de la cara
      vercara(event: any) {
        this.giro = event.target.value;
        if ( this.giro === 'Arriba') {
          this.giro = 'girarojo';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
        if ( this.giro === 'Abajo') {
          this.giro = 'giranaranja';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
        if ( this.giro === 'Izquierda') {
          this.giro = 'giraverde';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
        if ( this.giro === 'Derecha') {
          this.giro = 'giraazul';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
        if ( this.giro === 'Atras') {
          this.giro = 'girablanco';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
        if ( this.giro === 'Frente') {
          this.giro = 'amarillo';
          localStorage.setItem('giro', JSON.stringify(this.giro));
        }
      }

      // metodo para guardar movimiento
      guardamovimiento(gira: string) {
        this.nuevomovimiento.push(new Movimiento(gira, this.Fila, this.Cara,  this.fila1, this.fila2));
        localStorage.setItem('movimientos', JSON.stringify(this.nuevomovimiento));
      }
      // metodo para desahacer los movimientos
      movimientoatras() {
        this.regresar = true;
        this.nuevomovimiento = JSON.parse(localStorage.getItem('movimientos') || '[]');
        if (this.nuevomovimiento.length !== 0) {
        const ultimo = this.nuevomovimiento[this.nuevomovimiento.length - 1];
        const gira = ultimo.gira;
        this.Cara = ultimo.cara;
        this.Fila = ultimo.posfila;
        this.fila1 = ultimo.numfila1;
        this.fila2 = ultimo.numfila2;
        localStorage.setItem('movimientos', JSON.stringify(this.nuevomovimiento.pop()));
        if (gira === 'izquierda') {
          this.giraderecha();
        } else if (gira === 'derecha') {
          this.giraizquierda();
        } else if (gira === 'arriba') {
          this.giraabajo();
        } else {
          this.giraarriba();
        }
      } else {
        localStorage.removeItem('movimientos');
        this.regresar = false;
      }
      }
      borramovimiento() {
        // si el regresa es igual a true no me guarda el ultimo movimiento
        if (this.regresar === true) {
          this.regresarojo = true;
          this.regresar = false;
          this.nuevomovimiento.pop();
          localStorage.setItem('movimientos', JSON.stringify(this.nuevomovimiento));
          this.Cara = this.Auxiliarcara;
        }
      }
      gurdarcubo() {
        this.nuevomovimiento = JSON.parse(localStorage.getItem('movimientos') || '[]');
        this.guardar = JSON.parse(localStorage.getItem('cubo') || '[]');
        if (this.guardar.length > 0 && this.restaura === true && this.guardanuevo === true) {
          for (let index = 0; index < this.nuevomovimiento.length; index++) {
            const ultimo = this.nuevomovimiento[index];
            const gira = ultimo.gira;
            this.Cara = ultimo.cara;
            this.Fila = ultimo.posfila;
            this.fila1 = ultimo.numfila1;
            this.fila2 = ultimo.numfila2;
            this.guardar.push(ultimo);
            localStorage.setItem('cubo', JSON.stringify(this.guardar));
            this.guardanuevo = false;
          }
        } else if (this.guardar.length > 0 && this.restaura === true && this.guardanuevo === false) {
            localStorage.setItem('cubo', JSON.stringify(this.guardar));
        } else {
          localStorage.removeItem('cubo');
          localStorage.setItem('cubo', JSON.stringify(this.nuevomovimiento));
        }
      }
      restaurar() {
        this.restaura = true;
        this.guardanuevo = false;
        this.nuevomovimiento = JSON.parse(localStorage.getItem('cubo') || '[]');
        if (this.nuevomovimiento !== []) {
        this.cubo = new Cubo;
          for (let index = 0; index < this.nuevomovimiento.length; index++) {
            const ultimo = this.nuevomovimiento[index];
            const gira = ultimo.gira;
            this.Cara = ultimo.cara;
            this.Fila = ultimo.posfila;
            this.fila1 = ultimo.numfila1;
            this.fila2 = ultimo.numfila2;
            if (gira === 'derecha') {
              this.giraderecha();
            } else if (gira === 'izquierda') {
              this.giraizquierda();
            } else if (gira === 'abajo') {
              this.giraabajo();
            } else {
              this.giraarriba();
            }
        this.nuevomovimiento.pop();
        localStorage.setItem('movimientos', JSON.stringify([]));
        }
      }
        this.Fila = 'Arriba';
        this.giro = this.nuevomovimiento = JSON.parse(localStorage.getItem('giro') || '[]');
        this.nuevomovimiento = [];
      }

}
